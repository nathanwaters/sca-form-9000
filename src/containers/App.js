import React from "react";
import SubscribeHeader from "components/Subscribe/Header";
import SubscribeForm from "components/Subscribe/Form";

import "normalize.css";
import styles from "./App.module.css";

const App = () => {
  return (
    <div className={styles.app}>
      <SubscribeHeader />
      <SubscribeForm endpoint="http://localhost:5000/api/subscribe" />
    </div>
  );
};

export default App;
