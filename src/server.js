const express = require("express");
const cors = require("cors");
const axios = require("axios");

const app = express();
const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`Listening on port ${port}`));

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//subscribe form endpoint
app.post("/api/subscribe", (req, res) => {
  axios({
    method: "post",
    url: "https://ckzvgrbymezqegu.form.io/reacttestform/submission",
    headers: {
      "Content-Type": "application/json",
      "x-auth": "react-test"
    },
    data: req.body
  })
    .then(body => {
      let data = body.data;
      res.send({
        status: "success",
        data: {
          state: data.state,
          id: data._id,
          form: data.form
        }
      });
    })
    .catch(error =>
      res.send({
        status: "error",
        message: error.message
      })
    );
});
