import React from "react";
import { render, cleanup, fireEvent, wait } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import axios from "axios";
import SubscribeForm from "../Form";

afterEach(cleanup);

it("renders without crashing", () => {
  render(<SubscribeForm />);
});

it("fires validation errors after submit click", async () => {
  const { getByTestId } = render(<SubscribeForm />);
  fireEvent.click(getByTestId("submit"));
  const error = await getByTestId("error-firstName");
  expect(error).toHaveTextContent("First name is required");
});

it("it passes validation error after mock firstName value", async () => {
  const { getByTestId } = render(<SubscribeForm />);
  const input = getByTestId("input-firstName");
  fireEvent.change(input, { target: { value: "Bob" } });
  fireEvent.click(getByTestId("submit"));
  const error = await getByTestId("error-firstName");
  expect(error).toBeEmpty();
});

it("shows thankyou message after successful submit", async () => {
  const { getByTestId, getByText } = render(
    <SubscribeForm endpoint="http://localhost:5000/api/subscribe" />
  );
  const firstName = getByTestId("input-firstName");
  fireEvent.change(firstName, { target: { value: "Bob" } });
  const lastName = getByTestId("input-lastName");
  fireEvent.change(lastName, { target: { value: "Jones" } });
  const email = getByTestId("input-email");
  fireEvent.change(email, { target: { value: "bob@jones.com" } });
  const mobilePhone = getByTestId("input-mobilePhone");
  fireEvent.change(mobilePhone, { target: { value: "0412345678" } });
  fireEvent.click(getByTestId("submit"));
  const success = await wait(() => getByText("Thank you for subscribing!"));
  expect(success).not.toBeNull();
});

it("successfully submits data to the API", async () => {
  await axios
    .post("http://localhost:5000/api/subscribe", {
      data: {
        firstName: "Bob",
        lastName: "Jones",
        email: "bob@jones.com",
        mobilePhone: "0412345678"
      }
    })
    .then(function(res) {
      expect(res.data.status).toBe("success");
      expect(res.data.id).not.toBeNull();
    });
});
