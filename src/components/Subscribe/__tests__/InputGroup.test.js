import React from "react";
import { render, cleanup, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import InputGroup from "../InputGroup";

afterEach(cleanup);

it("renders without crashing", () => {
  render(<InputGroup />);
});

it("renders correctly", () => {
  const { getByTestId } = render(
    <InputGroup
      key="firstName"
      label="First Name"
      name="firstName"
      type="text"
      val="Bob"
      errors="An error"
      required={true}
    />
  );
  expect(getByTestId("label")).toHaveTextContent("First Name");
  expect(getByTestId("input-firstName")).toHaveAttribute("type", "text");
  expect(getByTestId("input-firstName")).toHaveValue("Bob");
  expect(getByTestId("input-firstName")).toBeRequired(true);
  expect(getByTestId("error-firstName")).toHaveTextContent("An error");
});

it("runs onchange correctly", () => {
  const onChange = jest.fn();
  const { getByTestId } = render(
    <InputGroup
      key="firstName"
      label="First Name"
      name="firstName"
      type="text"
      handleChange={onChange}
      val="Bob"
      errors="An error"
      required={true}
    />
  );
  const input = getByTestId("input-firstName");
  fireEvent.change(input, {
    target: { value: "Mary" }
  });
  expect(onChange).toHaveBeenCalledTimes(1);
});
