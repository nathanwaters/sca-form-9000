import React from "react";
import PropTypes from "prop-types";

import styles from "./InputGroup.module.css";

const InputGroup = props => {
  return (
    <div className={styles.inputGroup}>
      <label data-testid="label" htmlFor={props.name}>
        {props.label} {props.required && <span>*</span>}
      </label>
      <input
        data-testid={"input-" + props.name}
        type={props.type}
        id={props.name}
        name={props.name}
        defaultValue={props.val || ""}
        onChange={props.handleChange}
        required={props.required}
      />
      <div data-testid={"error-" + props.name} className={styles.error}>
        {props.errors}
      </div>
    </div>
  );
};

InputGroup.propTypes = {
  label: PropTypes.string,
  name: PropTypes.string,
  type: PropTypes.string,
  handleChange: PropTypes.func,
  val: PropTypes.string,
  errors: PropTypes.string,
  required: PropTypes.bool
};

export default InputGroup;
