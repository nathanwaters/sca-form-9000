import React from "react";
import styles from "./Header.module.css";

const SubscribeHeader = () => {
  return (
    <div className={styles.header}>
      <div className={styles.container}>
        <h3 className={styles.title}>Subscribe</h3>
      </div>
    </div>
  );
};

export default SubscribeHeader;
