import React, { useState } from "react";
import PropTypes from "prop-types";
import axios from "axios";

import InputGroup from "./InputGroup";
import useForm from "utils/useForm";
import validate from "utils/validateSubscribeForm";

import styles from "./Form.module.css";

const SubscribeForm = props => {
  const [subscribed, setSubscribed] = useState(null);
  const [isSubmitting, setIsSubmitting] = useState(false);
  const { values: user, errors, handleChange, handleSubmit } = useForm(
    subscribe,
    validate
  );
  const formFields = [
    {
      label: "First Name",
      name: "firstName",
      type: "text",
      required: true
    },
    {
      label: "Last Name",
      name: "lastName",
      type: "text",
      required: true
    },
    {
      label: "Email",
      name: "email",
      type: "email",
      required: true
    },
    {
      label: "Mobile Phone",
      name: "mobilePhone",
      type: "tel",
      required: false
    }
  ];

  //validation success, post data via API
  function subscribe() {
    setIsSubmitting(true);
    axios
      .post(props.endpoint, {
        data: user
      })
      .then(res => {
        setSubscribed(res.data);
        setIsSubmitting(false);
      })
      .catch(error => {
        console.log(error);
      });
  }

  return (
    <div className={styles.container}>
      <div className={styles.formWrap}>
        {subscribed && subscribed.status === "success" ? (
          <h2 className={styles.success}>Thank you for subscribing!</h2>
        ) : (
          <form className={styles.form} onSubmit={handleSubmit} noValidate>
            {subscribed && subscribed.status === "error" && (
              <div className={styles.error}>{subscribed.message}</div>
            )}
            {formFields.map(x => (
              <InputGroup
                key={x.name}
                label={x.label}
                name={x.name}
                type="text"
                handleChange={handleChange}
                val={user[x.name]}
                errors={errors[x.name]}
                required={x.required}
              />
            ))}
            <button
              data-testid="submit"
              className={styles.submit}
              type="submit"
            >
              {isSubmitting ? "Submitting..." : "Submit"}
            </button>
          </form>
        )}
      </div>
    </div>
  );
};

SubscribeForm.propTypes = {
  endpoint: PropTypes.string
};

export default SubscribeForm;
