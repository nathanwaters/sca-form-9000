const validate = user => {
  let errors = {};

  //first name missing
  if (!user.firstName) {
    errors.firstName = "First name is required";
  }

  //last name missing
  if (!user.lastName) {
    errors.lastName = "Last name is required";
  }

  //email missing/invalid
  if (!user.email) {
    errors.email = "Email address is required";
  } else if (!/\S+@\S+\.\S+/.test(user.email)) {
    errors.email = "Email address is invalid";
  }

  //mobile invalid
  if (user.mobilePhone && !/^(\+\d{1,3}[- ]?)?\d{10}$/.test(user.mobilePhone)) {
    errors.mobilePhone = "Mobile is invalid";
  }

  return errors;
};

export default validate;
